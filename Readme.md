# WebSdg

WebSdg es una rest api de ejemplo para probar conocimientos t�cnicos

## Instalaci�n

Abrir soluci�n con visual estudio, descargar paquetes nuget, compilar y ejecutar la aplicaci�n.
Se abrir� el navegador web seleccionado y mostrar� la p�gina principal de Swagger que mostrar�
la lista de Controllers y endpoints configurados

## Usage

Hay dos controllers: ReaderController y PublisherController.

### ReaderController
- A�adir readers (llamada post addReader).
	Este post tiene un body: 
	{
	  "Id": 0,
	  "Name": "string",
	  "IsSubscribed": true
	}
	No es necesario introducir el Id, pero si el campo Name para el nombre del reader.
- Obtener la lista de readers (Get).
- Modificar un reader (put). En esta llamada si es necesario introducir el Id y el nombre del reader.

### PublisherController
- A�adir publisher (llamada post addNotification). Recibe por par�metro el string con la notificaci�n a insertar.
- Obtener la lista de notificaciones (Get).

## UnitTest y Logs

Hay dos proyectos de pruebas unitarias, uno para la capa de Business Logic (o servicio) y otro para la api.