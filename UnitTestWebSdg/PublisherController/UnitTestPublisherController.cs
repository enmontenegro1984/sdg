﻿using BusinessLogic.Models;
using BusinessLogic.Services;
using CommonHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using WebApplication1.Controllers;

namespace UnitTestWebSdg
{
    [TestClass]
    public class UnitTestPublisherController
    {
        private PublisherController _controller;

        [TestInitialize]
        public void SetUp()
        {
            //Initialize controller
            _controller = new PublisherController
            {
                Request = new HttpRequestMessage()
            };
            _controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            _controller.Request.RequestUri = new Uri("http://localhost/api/Sample");
            _controller.TestMode = true;
        }

        [TestCleanup]
        public void TearDown()
        {
            //Destroy controller
            _controller = null;
        }

        #region GetSubscribers

        [TestMethod]
        public void UT_API_Publisher_GetSubscribers_OK()
        {
            var mock = new Mock<INotificationService>();
            var dataReturnedByService = new SdgResponse<List<Reader>>();
            dataReturnedByService.Data = new List<Reader>();
            dataReturnedByService.ErrorCode = SdgResponseBodyErrorCode.NoError;

            mock.Setup(bl => bl.GetSubscribers()).Returns(dataReturnedByService);

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.GetSubscribers() as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.OK, response.Response.StatusCode);
        }

        [TestMethod]
        public void UT_API_Publisher_GetSubscribers_BadRequest()
        {
            var mock = new Mock<INotificationService>();
            var dataReturnedByService = new SdgResponse<List<Reader>>();
            dataReturnedByService.Data = new List<Reader>();
            dataReturnedByService.ErrorCode = SdgResponseBodyErrorCode.InvalidParameters;

            mock.Setup(bl => bl.GetSubscribers()).Returns(dataReturnedByService);

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.GetSubscribers() as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.BadRequest, response.Response.StatusCode);
        }

        [TestMethod]
        public void UT_API_Publisher_GetSubscribers_BlThrowsException()
        {
            var mock = new Mock<INotificationService>();

            mock.Setup(bl => bl.GetSubscribers()).Throws(new Exception());

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.GetSubscribers() as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.InternalServerError, response.Response.StatusCode);
        }

        #endregion

        #region AddNotification

        [TestMethod]
        public void UT_API_Publisher_AddNotification_OK()
        {
            var mock = new Mock<INotificationService>();
            var dataReturnedByService = new SdgResponse<bool>();
            dataReturnedByService.Data = true;
            dataReturnedByService.ErrorCode = SdgResponseBodyErrorCode.NoError;
            string inputMessage = "test";

            mock.Setup(bl => bl.AddNotification(inputMessage)).Returns(dataReturnedByService);

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.AddNotification(inputMessage) as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.OK, response.Response.StatusCode);
        }

        [TestMethod]
        public void UT_API_Publisher_AddNotification_BadRequest()
        {
            var mock = new Mock<INotificationService>();
            var dataReturnedByService = new SdgResponse<bool>();
            dataReturnedByService.Data = false;
            dataReturnedByService.ErrorCode = SdgResponseBodyErrorCode.InvalidParameters;
            string inputMessage = "test";

            mock.Setup(bl => bl.AddNotification(inputMessage)).Returns(dataReturnedByService);

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.AddNotification(inputMessage) as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.BadRequest, response.Response.StatusCode);
        }

        [TestMethod]
        public void UT_API_Publisher_AddNotification_BlThrowsException()
        {
            var mock = new Mock<INotificationService>();
            var inputMessage = "test";

            mock.Setup(bl => bl.AddNotification(inputMessage)).Throws(new Exception());

            _controller.SetBusinessLogic(mock.Object);

            var response = _controller.AddNotification(inputMessage) as ResponseMessageResult;

            Assert.AreEqual(HttpStatusCode.InternalServerError, response.Response.StatusCode);
        }

        #endregion
    }
}
