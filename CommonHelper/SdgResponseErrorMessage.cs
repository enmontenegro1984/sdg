﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public class SdgResponseErrorMessage
    {
        public static string INTERNAL_ERROR = "Internal Error";
        public static string NO_CONTENT = "No Content";
        public static string INVALID_PARAMETERS = "Error with parameters. They are not valid.";
        public static string NOT_FOUND = "Element Not Found";
    }
}
