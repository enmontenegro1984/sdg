﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public enum SdgResponseBodyErrorCode
    {
        NoError = 0,
        InternalError = 1,
        NoContent = 2,
        InvalidParameters = 3,
        NotFound = 4,
        ExistingItem = 5
    }
}
