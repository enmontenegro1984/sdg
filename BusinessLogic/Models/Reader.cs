﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class Reader
    {
        public Reader()
        {
        }

        public Reader(string Name, bool IsSubscribed)
        {
            this.Name = Name;
            this.IsSubscribed = IsSubscribed;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSubscribed { get; set; }
    }
}
