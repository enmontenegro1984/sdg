﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public static class NotificationsModel
    {
        public static List<string> Messages { get; set; }

        public static bool IsValid()
        {
            bool result = false;
            if(Messages.Count > 0)
            {
                result = Messages.All(m => !string.IsNullOrEmpty(m));
            }

            return result;
        }

        public static void AddMessage(string message)
        {
            if (Messages == null)
                Messages = new List<string>();

            Messages.Add(message);
        }
    }
}
