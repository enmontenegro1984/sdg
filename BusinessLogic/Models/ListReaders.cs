﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public static class ListReaders
    {
        public static List<Reader> Readers { get; set; }

        public static Reader GetReader(string name)
        {
            Reader readerToReturn = null;

            if (Readers != null)
                readerToReturn = Readers.FirstOrDefault(r => r.Name == name);

            return readerToReturn;
        }

        public static void AddReader(Reader reader)
        {
            if (Readers == null)
                Readers = new List<Reader>();

            Readers.Add(reader);
        }
    }
}
