﻿using BusinessLogic.Models;
using CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public interface INotificationService
    {
        SdgResponse<List<string>> GetNotifications(string user);
        SdgResponse<List<Reader>> GetSubscribers();
        SdgResponse<int> AddReader(Reader reader);
        SdgResponse<bool> ModifyReader(Reader reader);
        SdgResponse<bool> AddNotification(string message);
    }
}
