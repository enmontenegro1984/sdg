﻿using BusinessLogic.Models;
using BusinessLogic.Services;
using CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class NotificationServiceImpl: INotificationService
    {
        public NotificationServiceImpl()
        {
            if (ListReaders.Readers == null)
                ListReaders.Readers = new List<Reader>();

            if (NotificationsModel.Messages == null)
                NotificationsModel.Messages = new List<string>();
        }

        public SdgResponse<List<string>> GetNotifications(string user)
        {
            SdgResponse<List<string>> result = new SdgResponse<List<string>>();
            result.Data = NotificationsModel.Messages;
            if (string.IsNullOrEmpty(user))
            {
                result.ErrorCode = SdgResponseBodyErrorCode.NotFound;
            }
            else
            {
                if (ListReaders.GetReader(user) != null)
                {
                    if (NotificationsModel.IsValid())
                        result.ErrorCode = SdgResponseBodyErrorCode.NoError;
                    else
                        result.ErrorCode = SdgResponseBodyErrorCode.NoContent;
                }
                else
                {
                    result.ErrorCode = SdgResponseBodyErrorCode.NotFound;
                }
            }            

            return result;
        }

        public SdgResponse<List<Reader>> GetSubscribers()
        {
            SdgResponse<List<Reader>> result = new SdgResponse<List<Reader>>();
            result.Data = ListReaders.Readers;
            if (ListReaders.Readers.Count == 0)
                result.ErrorCode = SdgResponseBodyErrorCode.NoContent;
            else
                result.ErrorCode = SdgResponseBodyErrorCode.NoError;
            return result;
        }

        public SdgResponse<int> AddReader(Reader reader)
        {
            SdgResponse<int> result = new SdgResponse<int>();
            var index = ListReaders.Readers.Any() ? ListReaders.Readers.Count + 1 : 1;
            result.Data = index;

            if (ListReaders.Readers.Any(s => s.Name.ToUpper() == reader.Name.ToUpper()))
            {
                result.ErrorCode = SdgResponseBodyErrorCode.ExistingItem;
            }
            else
            {
                ListReaders.Readers.Add(reader);
                result.ErrorCode = SdgResponseBodyErrorCode.NoError;
            }

            return result;
        }

        public SdgResponse<bool> ModifyReader(Reader reader)
        {
            SdgResponse<bool> result = new SdgResponse<bool>();
            result.Data = false;
            result.ErrorCode = SdgResponseBodyErrorCode.NotFound;

            if (ListReaders.Readers.Any(s => s.Id == reader.Id))
            {
                ListReaders.Readers.FirstOrDefault(s => s.Id == reader.Id).IsSubscribed = reader.IsSubscribed;
                ListReaders.Readers.FirstOrDefault(s => s.Id == reader.Id).Name = reader.Name;
                result.Data = true;
                result.ErrorCode = SdgResponseBodyErrorCode.NoError;
            }

            return result;
        }

        public SdgResponse<bool> AddNotification(string message)
        {
            SdgResponse<bool> result = new SdgResponse<bool>();
            result.Data = false;
            result.ErrorCode = SdgResponseBodyErrorCode.ExistingItem;

            var notificationExists = NotificationsModel.Messages.Any(m => m == message);
            if(!notificationExists)
            {
                NotificationsModel.Messages.Add(message);
                result.ErrorCode = SdgResponseBodyErrorCode.NoError;
                result.Data = true;
            }

            return result;
        }
    }
}
