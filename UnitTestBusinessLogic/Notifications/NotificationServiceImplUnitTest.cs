﻿using BusinessLogic;
using BusinessLogic.Models;
using CommonHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTestBusinessLogic
{
    [TestClass]
    public class NotificationServiceImplUnitTest
    {
        private NotificationServiceImpl _businessLogic;

        [TestInitialize]
        public void SetUp()
        {
            _businessLogic = new NotificationServiceImpl {  };
        }

        [TestCleanup]
        public void TearDown()
        {
            _businessLogic = null;
        }

        #region GetNotifications

        [TestMethod]
        public void UT_BL_Add_Notification_And_GetAllNotifications_Ok()
        {
            //ARRANGE
            var message = "Nueva notificación";
            var resultAddMessage = _businessLogic.AddNotification(message);
            var newReader = new Reader("test", true);
            var result = new SdgResponse<List<string>>();

            //ACT
            var resultAddReader = _businessLogic.AddReader(newReader);
            if(resultAddMessage.ErrorCode == CommonHelper.SdgResponseBodyErrorCode.NoError && resultAddReader.ErrorCode == CommonHelper.SdgResponseBodyErrorCode.NoError)
            {
                result = _businessLogic.GetNotifications(newReader.Name);
            }

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NoError);
            Assert.IsTrue(result.Data.Exists(m => m.Contains(message)));
        }

        [TestMethod]
        public void UT_BL_With_No_Notifications_GetAllNotifications_Empty()
        {
            //ARRANGE
            var newReader = new Reader("test", true);
            var result = new SdgResponse<List<string>>();

            //ACT
            var resultAddReader = _businessLogic.AddReader(newReader);
            if (resultAddReader.ErrorCode == CommonHelper.SdgResponseBodyErrorCode.NoError)
            {
                result = _businessLogic.GetNotifications(newReader.Name);
            }

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NoContent);
            Assert.IsTrue(result.Data.Count == 0);
        }

        #endregion

        #region GetSubscribers

        [TestMethod]
        public void UT_BL_GetSubscribers_WithOneSubscriber_Ok()
        {
            //ARRANGE            
            var result = new SdgResponse<List<Reader>>();
            var newReader = new Reader("test", true);
            var resultAddSubscriber = _businessLogic.AddReader(newReader);

            //ACT
            if(resultAddSubscriber.ErrorCode == SdgResponseBodyErrorCode.NoError)
            {
                result = _businessLogic.GetSubscribers();
            }            

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NoError);
            Assert.AreEqual(result.Data[0].Name, newReader.Name);
            Assert.AreEqual(result.Data[0].IsSubscribed, newReader.IsSubscribed);
        }

        [TestMethod]
        public void UT_BL_GetSubscribers_WithNoSubscribers_NoContent()
        {
            //ARRANGE            
            var result = new SdgResponse<List<Reader>>();

            //ACT
            result = _businessLogic.GetSubscribers();

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NoContent);
        }

        #endregion

        #region AddReader

        [TestMethod]
        public void UT_BL_AddReader_AddNonExistingReader_Ok()
        {
            //ARRANGE            
            var newReader = new Reader("test", true);

            //ACT
            var resultAddSubscriber = _businessLogic.AddReader(newReader);

            //ASSERT
            Assert.IsNotNull(resultAddSubscriber);
            Assert.AreEqual(resultAddSubscriber.ErrorCode, SdgResponseBodyErrorCode.NoError);
            Assert.AreEqual(resultAddSubscriber.Data, 1);
        }

        [TestMethod]
        public void UT_BL_AddReader_AddExistingReader_ExistingItem()
        {
            //ARRANGE            
            var newReader = new Reader("test", true);

            //ACT
            var resultAddSubscriber = _businessLogic.AddReader(newReader);
            var result = _businessLogic.AddReader(newReader);

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.ExistingItem);
        }

        #endregion

        #region ModifyReader

        [TestMethod]
        public void UT_BL_ModifyReader_ModifyExistingReader_Ok()
        {
            //ARRANGE            
            var newReader = new Reader("test", true);

            //ACT
            var resultAddSubscriber = _businessLogic.AddReader(newReader);
            newReader.Id = resultAddSubscriber.Data;
            var result = _businessLogic.ModifyReader(newReader);

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NoError);
            Assert.AreEqual(result.Data, true);
        }

        [TestMethod]
        public void UT_BL_ModifyReader_ModifyNonExistingReader_NotFound()
        {
            //ARRANGE            
            var newReader = new Reader("test", true);

            //ACT
            var result = _businessLogic.ModifyReader(newReader);

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.NotFound);
            Assert.AreEqual(result.Data, false);
        }

        #endregion

        #region AddNotification

        [TestMethod]
        public void UT_BL_AddNotification_AddNonExistingNotification_Ok()
        {
            //ARRANGE            
            var message = "test";

            //ACT
            var resultAddNotification = _businessLogic.AddNotification(message);

            //ASSERT
            Assert.IsNotNull(resultAddNotification);
            Assert.AreEqual(resultAddNotification.ErrorCode, SdgResponseBodyErrorCode.NoError);
            Assert.AreEqual(resultAddNotification.Data, true);
        }

        [TestMethod]
        public void UT_BL_AddNotification_AddExistingNotification_ExistingItem()
        {
            //ARRANGE            
            var message = "test";

            //ACT
            var resultAddNotification = _businessLogic.AddNotification(message);
            var result = _businessLogic.AddNotification(message);

            //ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorCode, SdgResponseBodyErrorCode.ExistingItem);
            Assert.AreEqual(result.Data, false);
        }

        #endregion
    }
}
