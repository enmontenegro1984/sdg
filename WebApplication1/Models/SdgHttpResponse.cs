﻿using CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WebApplication1.Models
{
    public class SdgHttpResponse<T> where T: class
    {
        public SdgHttpResponse()
        {

        }

        public T Response { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }

        public void SetOkResponse(T data)
        {
            Response = data;
            StatusCode = HttpStatusCode.OK;
            Message = "";
        }

        public void SetResponseWithError(T data, SdgResponseBodyErrorCode errorCode)
        {
            Response = data;
            StatusCode = GetStatusCode(errorCode);
            Message = GetMessage(errorCode);
        }

        private HttpStatusCode GetStatusCode(SdgResponseBodyErrorCode errorCode)
        {
            switch(errorCode)
            {
                case SdgResponseBodyErrorCode.InternalError:
                    return HttpStatusCode.InternalServerError;
                case SdgResponseBodyErrorCode.NoContent:
                    return HttpStatusCode.NoContent;
                case SdgResponseBodyErrorCode.InvalidParameters:
                    return HttpStatusCode.BadRequest;
                case SdgResponseBodyErrorCode.NotFound:
                    return HttpStatusCode.NotFound;
                default:
                    return HttpStatusCode.InternalServerError;
            }
        }

        private string GetMessage(SdgResponseBodyErrorCode errorCode)
        {
            switch (errorCode)
            {
                case SdgResponseBodyErrorCode.InternalError:
                    return SdgResponseErrorMessage.INTERNAL_ERROR;
                case SdgResponseBodyErrorCode.NoContent:
                    return SdgResponseErrorMessage.NO_CONTENT;
                case SdgResponseBodyErrorCode.InvalidParameters:
                    return SdgResponseErrorMessage.INVALID_PARAMETERS;
                case SdgResponseBodyErrorCode.NotFound:
                    return SdgResponseErrorMessage.NOT_FOUND;
                default:
                    return SdgResponseErrorMessage.INTERNAL_ERROR;
            }
        }
    }
}