﻿using BusinessLogic;
using BusinessLogic.Models;
using BusinessLogic.Services;
using CommonHelper;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [RoutePrefix("Reader")]
    public class ReaderController : SdgController
    {       
        private INotificationService _notificationService;
        private static readonly NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();
        public override void InitializeBusinessLogic()
        {
            _notificationService = new NotificationServiceImpl();
        }

        public ReaderController()
        {

        }

        /// <summary>
        /// Gets the list of notifications for a determined user 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Returns the list of notifications for a determined user ", typeof(SdgResponse<List<string>>))]
        public IHttpActionResult GetNotifications(string user)
        {
            InitializeBusinessLogic();
            _log.Info("Called GetNotifications method from ReaderController");

            var response = new SdgHttpResponse<SdgResponse<List<string>>>();
            var notifications = new SdgResponse<List<string>>();
            try
            {
                notifications = _notificationService.GetNotifications(user);
                if (notifications.ErrorCode == SdgResponseBodyErrorCode.NoError)
                    response.SetOkResponse(notifications);
                else
                    response.SetResponseWithError(notifications, notifications.ErrorCode);
            }
            catch(Exception ex)
            {
                _log.Error("Error calling GetNotifications: " + ex.Message);
                response.SetResponseWithError(notifications, SdgResponseBodyErrorCode.InternalError);
            }            

            return ResponseMessage(Request.CreateResponse(response.StatusCode, response.Response));
        }

        /// <summary>
        /// Adds a reader to an internal list of readers
        /// </summary>
        /// <param name="reader"></param>
        /// <returns>Result of the addition</returns>
        [HttpPost]
        [Route("addReader")]
        public IHttpActionResult AddReader(Reader reader)
        {
            InitializeBusinessLogic();
            _log.Info("Called AddReader method from ReaderController");
            var response = new SdgHttpResponse<SdgResponse<int>>();
            var added = new SdgResponse<int>();

            try
            {
                added = _notificationService.AddReader(reader);
                if (added.ErrorCode == SdgResponseBodyErrorCode.NoError)
                    response.SetOkResponse(added);
                else
                    response.SetResponseWithError(added, added.ErrorCode);
            }
            catch (Exception ex)
            {
                _log.Error("Error calling AddReader: " + ex.Message);
                response.SetResponseWithError(added, SdgResponseBodyErrorCode.InternalError);
            }

            return ResponseMessage(Request.CreateResponse(response.StatusCode, response.Response));
        }

        /// <summary>
        /// Modifys a reader based on its Id
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Result of the modification</returns>
        [HttpPut]
        public IHttpActionResult ModifyReader(Reader reader)
        {
            InitializeBusinessLogic();
            _log.Info("Called ModifyReader method from ReaderController");

            var response = new SdgHttpResponse<SdgResponse<bool>>();
            var subscribed = new SdgResponse<bool>();
            try
            {
                subscribed = _notificationService.ModifyReader(reader);
                if (subscribed.ErrorCode == SdgResponseBodyErrorCode.NoError)
                    response.SetOkResponse(subscribed);
                else
                    response.SetResponseWithError(subscribed, subscribed.ErrorCode);
            }
            catch (Exception ex)
            {
                _log.Error("Error calling ModifyReader: " + ex.Message);
                response.SetResponseWithError(subscribed, SdgResponseBodyErrorCode.InternalError);
            }

            return ResponseMessage(Request.CreateResponse(response.StatusCode, response.Response));
        }
    }
}
