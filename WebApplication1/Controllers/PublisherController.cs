﻿using BusinessLogic;
using BusinessLogic.Models;
using BusinessLogic.Services;
using CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [RoutePrefix("Publisher")]
    public class PublisherController : SdgController
    {
        private INotificationService _notificationService;
        private static readonly NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();
        public override void InitializeBusinessLogic()
        {
            if(!TestMode)
                _notificationService = new NotificationServiceImpl();
        }

        public void SetBusinessLogic(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        public PublisherController()
        {

        }

        /// <summary>
        /// Gets the list of subscribers 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetSubscribers()
        {
            InitializeBusinessLogic();
            _log.Info("Called GetSubscribers method from PublisherController");

            var response = new SdgHttpResponse<SdgResponse<List<Reader>>>();
            var subscribers = new SdgResponse<List<Reader>>();
            try
            {
                subscribers = _notificationService.GetSubscribers();
                if (subscribers.ErrorCode == SdgResponseBodyErrorCode.NoError)
                    response.SetOkResponse(subscribers);
                else
                    response.SetResponseWithError(subscribers, subscribers.ErrorCode);
            }
            catch (Exception ex)
            {
                _log.Error("Error calling GetSubscribers: " + ex.Message);
                response.SetResponseWithError(subscribers, SdgResponseBodyErrorCode.InternalError);
            }

            return ResponseMessage(Request.CreateResponse(response.StatusCode, response.Response));
        }

        /// <summary>
        /// Adds a notification
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Result of the addition</returns>
        [HttpPost]
        [Route("addNotification")]
        public IHttpActionResult AddNotification(string message)
        {
            InitializeBusinessLogic();
            _log.Info("Called AddNotification method from PublisherController");
           
            var response = new SdgHttpResponse<SdgResponse<bool>>();
            var added = new SdgResponse<bool>();
            try
            {
                added = _notificationService.AddNotification(message);
                if (added.ErrorCode == SdgResponseBodyErrorCode.NoError)
                    response.SetOkResponse(added);
                else
                    response.SetResponseWithError(added, added.ErrorCode);
            }
            catch (Exception ex)
            {
                _log.Error("Error calling AddNotification: " + ex.Message);
                response.SetResponseWithError(added, SdgResponseBodyErrorCode.InternalError);
            }

            return ResponseMessage(Request.CreateResponse(response.StatusCode, response.Response));
        }       
    }
}
