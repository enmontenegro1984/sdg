﻿using CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public abstract class SdgController : ApiController
    {
        public bool TestMode { get; set; }
        public abstract void InitializeBusinessLogic();

        protected SdgController()
        {
        }
    }
}
